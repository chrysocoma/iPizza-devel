// -*- coding: utf-8-unix -*-
/*!
  @file			NuCL_Slave.hpp
  @brief		Nerve-Microcontroller connect Layer Slave Part

  @author		T.Kawamura
  @version	1.1
  @date		2017-06-24	新規作成
  @date		2017-08-24	バグ修正
	@date		2017-09-29	パケット間タイムアウト追加

  @copyright	2017(C) T.Kawamura
*/
#ifndef NUCL_SLAVE_H
#define NUCL_SLAVE_H

#include "mbed.h"
#include "AsyncSerial.hpp"

#define NUCL_HEADER 0x40
#define NUCL_FOOTER 0x0A
#define NUCL_TIMEOUT 1000

const int NUCL_SLAVE_MAX_PACKET_SIZE = 128;

enum loadstate_e{
  STATE_WAIT_HEADER,
  STATE_WAIT_LENGTH,
  STATE_WAIT_PAYLOAD,
  STATE_WAIT_CHECKSUM,
  STATE_WAIT_FOOTER
};

enum update_result_e{
  NOT_READY_FOR_GET_PAYLOAD,
  READY_FOR_GET_PAYLOAD
};

using namespace std;

/*!
  @class	NuCL_Slave
  @brief	Recieve NuCL packet.
*/
class NuCL_Slave{
private:
  AsyncSerial *serial_port;
  
  //Debug
  //DigitalOut *led;
	//AsyncSerial *pc;
	Timer *time_out_timer;
	
  loadstate_e load_status;
  
  uint8_t packet_buffer[NUCL_SLAVE_MAX_PACKET_SIZE];
  
  uint32_t size_of_payload;
  
  uint8_t payload_data_count;
  
  uint8_t recieved_payload[NUCL_SLAVE_MAX_PACKET_SIZE];
  
public:
  
  NuCL_Slave(PinName rxpin, uint32_t payload_size, uint32_t baudrate=115200, uint32_t uart_fifo_buffer_size=256);
  
  virtual ~NuCL_Slave(void);
  
  virtual update_result_e update(void);
  
  virtual uint8_t get_payload_by_1byte(uint8_t index);
};

#endif

