 # -*- coding:utf-8-unix -*-

# System modules
import sys
import time
import RPi.GPIO as GPIO
from RPLCD.gpio import CharLCD

# Original modules
import ipizza_detect as ipd
import ipizza_ctrl as ipc
import degree_math as dmath
import math

Cutspeed = 200          # RPM
Movespeed = 200         # RPM

def main():
    # Initializing
    lcd = CharLCD(pin_rs=19, pin_rw=21, pin_e=23, pins_data=[35, 29, 31, 33], numbering_mode=GPIO.BOARD, cols=16, rows=2, auto_linebreaks=True)
    lcd.clear()
    lcd.write_string('Initializing...\n')
    GPIO.setmode(GPIO.BOARD)
    GPIO.setwarnings(False)
    swstart = 3
    rsw4 = 5
    rsw1 = 7
    rsw8 = 11
    rsw2 = 13
    GPIO.setup(swstart, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    GPIO.setup(rsw4, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    GPIO.setup(rsw1, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    GPIO.setup(rsw8, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    GPIO.setup(rsw2, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    
    ctrl = ipc.iPizza_Ctrl()
    
    # Detect the pizza
    ctrl.move(300, 0, 200, 200)
    
    lcd.clear()
    lcd.write_string('Detecting...\n')
    result = ipd.DetectPizza(0)
    
    if result is 0:
        print('Pizza not found')
        lcd.write_string('Pizza not found')
        sys.exit()
        
    # Get X, Y & R of the pizza
    (xpizza, ypizza, rpizza) = result
    lcd.clear()
    lcd.write_string('Found a pizza!  Push button')
    
    while True:
        if GPIO.input(swstart) is 0:
            break
    
    # Get number of cut and start
    bit3 = ((1, 0)[GPIO.input(rsw8)]) << 3
    bit2 = ((1, 0)[GPIO.input(rsw4)]) << 2
    bit1 = ((1, 0)[GPIO.input(rsw2)]) << 1
    bit0 = ((1, 0)[GPIO.input(rsw1)])
    npizza = bit3 | bit2 | bit1 | bit0
    lcd.clear()
    lcd.write_string('Cut: {0}\nCalculating...'.format(npizza))
    
    # Calculate
    deg_array = []
    x_array = []
    y_array = []
    xrpm_array = []
    yrpm_array = []
    
    for i in range(npizza):
        deg_array.append(int(round(90 + 360 * i / npizza)))
        x_array.append(int(round(xpizza + rpizza * dmath.cos(deg_array[-1]))))
        y_array.append(int(round(ypizza + rpizza * dmath.sin(deg_array[-1]))))

        # tan
        xytan = abs(dmath.tan(deg_array[-1]))

        if deg_array[-1] % 90 == 0:
            xytan = 1
        
        print('xytan', xytan)
        
        if xytan > 1:
            yrpm_array.append(int(Cutspeed))
            xrpm_array.append(int(round(Cutspeed / xytan) + 0))
        elif xytan < 1:
            xrpm_array.append(int(Cutspeed))
            yrpm_array.append(int(round(Cutspeed * xytan) + 0))
        else:
            xrpm_array.append(int(Cutspeed))
            yrpm_array.append(int(Cutspeed))
        
        print('Deg[{0}]: {1}\n'.format(i, deg_array[-1]))
        print('MYCoor(SXCoor)[{0}]: {1}\n'.format(i, x_array[-1]))
        print('MXCoor(SYCoor)[{0}]: {1}\n'.format(i, y_array[-1]))
        print('MYCoor(SXCoor) RPM[{0}]: {1}\n'.format(i, xrpm_array[-1]))
        print('MXCoor(SYCoor) RPM[{0}]: {1}\n'.format(i, yrpm_array[-1]))
    
    # Cut
    lcd.clear()
    lcd.write_string('Cutting...      ')
    for i in range(npizza):
        ctrl.move(x_array[i], y_array[i], Movespeed, Movespeed)
        ctrl.rotate(deg_array[i])
        ctrl.setCutter(True)
        ctrl.move(xpizza, ypizza, xrpm_array[i], yrpm_array[i])
        ctrl.move(x_array[i], y_array[i], xrpm_array[i], yrpm_array[i])
        ctrl.move(xpizza, ypizza, xrpm_array[i], yrpm_array[i])
        ctrl.move(x_array[i], y_array[i], xrpm_array[i], yrpm_array[i])
        ctrl.setCutter(False)

    ctrl.move(0, 0, 200, 200)
    ctrl.rotate(0)
    ctrl.finish()
    
    lcd.close(clear=True)

if  __name__  == '__main__':
    main()
